package com.example.ivitruk.vkadditionalclient.di.component;

import com.example.ivitruk.vkadditionalclient.di.module.BusinessModule;
import com.example.ivitruk.vkadditionalclient.di.module.ViewModelModule;
import com.example.ivitruk.vkadditionalclient.di.scope.ViewModelScope;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;

import dagger.Component;
import dagger.Subcomponent;

@ViewModelScope
@Subcomponent(modules = BusinessModule.class)
public interface BusinessComponent {
    ViewModelComponent plus(ViewModelModule viewModelModule);
}
