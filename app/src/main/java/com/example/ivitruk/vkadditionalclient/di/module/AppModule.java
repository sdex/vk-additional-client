package com.example.ivitruk.vkadditionalclient.di.module;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.di.scope.ApplicationScope;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IFragmentNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.manager.impl.FragmentNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.impl.SessionManager;
import com.example.ivitruk.vkadditionalclient.manager.impl.ActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.impl.InfoManager;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @ApplicationScope
    App providesApp() {
        return app;
    }

    @Provides
    @ApplicationScope
    ISessionManager providesSessionManager(App app) {
        return new SessionManager(app);
    }

    @Provides
    @ApplicationScope
    OkHttpClient providesOkHttpClient(ISessionManager sessionManager) {
        return new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    HttpUrl url = chain.request().url()
                            .newBuilder()
                            .addQueryParameter("v", ApiData.API_VERSION)
                            .addQueryParameter("access_token", sessionManager.getAccessToken())
                            .build();
                    Request request = chain.request().newBuilder().url(url).build();
                    return chain.proceed(request);
                }).build();
    }

    @Provides
    @ApplicationScope
    Retrofit providesRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(ApiData.VK_API_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @ApplicationScope
    IInfoManager providesInfoManager() {
        return new InfoManager();
    }

    @Provides
    @ApplicationScope
    IActivityNavigationManager activityNavigationManager(App app) {
        return new ActivityNavigationManager(app);
    }

    @Provides
    @ApplicationScope
    IFragmentNavigationManager fragmentNavigationManager() {
        return new FragmentNavigationManager();
    }

    @Provides
    ConnectivityReceiver connectivityReceiver() {
        return new ConnectivityReceiver();
    }
}