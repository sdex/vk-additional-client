package com.example.ivitruk.vkadditionalclient.di.module;

import android.content.Context;

import com.example.ivitruk.vkadditionalclient.di.scope.ViewScope;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IFragmentNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IFriendsBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.ILikesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IMessagesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IUsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;
import com.example.ivitruk.vkadditionalclient.viewmodel.DrawerItemViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.FollowersViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.FriendsViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.LoginViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.MainViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.MessageItemViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.MessagesViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.SplashViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.UserItemViewModel;
import com.example.ivitruk.vkadditionalclient.viewmodel.UsersSearchViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {

    private Context context;

    public ViewModelModule(Context context) {
        this.context = context;
    }

    @Provides
    @ViewScope
    MessagesViewModel providesMessagesViewModel(IAuthBusinessLayer authBusinessLayer,
                                                ConnectivityReceiver connectivityReceiver,
                                                ISessionManager sessionManager,
                                                IInfoManager infoManager,
                                                IActivityNavigationManager activityNavigationManager,
                                                IMessagesBusinessLayer messagesBusinessLayer) {
        return new MessagesViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager, messagesBusinessLayer);
    }

    @Provides
    @ViewScope
    LoginViewModel providesLoginViewModel(IAuthBusinessLayer authBusinessLayer,
                                          ConnectivityReceiver connectivityReceiver,
                                          ISessionManager sessionManager,
                                          IInfoManager infoManager,
                                          IActivityNavigationManager activityNavigationManager) {
        return new LoginViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    @Provides
    @ViewScope
    MainViewModel providesMainViewModel(IAuthBusinessLayer authBusinessLayer,
                                        ConnectivityReceiver connectivityReceiver,
                                        ISessionManager sessionManager,
                                        IInfoManager infoManager,
                                        IActivityNavigationManager activityNavigationManager,
                                        IFragmentNavigationManager fragmentNavigationManager) {
        return new MainViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager, fragmentNavigationManager);
    }

    @Provides
    @ViewScope
    MessageItemViewModel providesMessageItemViewModel(IAuthBusinessLayer authBusinessLayer,
                                                      ConnectivityReceiver connectivityReceiver,
                                                      ISessionManager sessionManager,
                                                      IInfoManager infoManager,
                                                      IActivityNavigationManager activityNavigationManager) {
        return new MessageItemViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    @Provides
    @ViewScope
    UserItemViewModel providesUserItemViewModel(IAuthBusinessLayer authBusinessLayer,
                                                ConnectivityReceiver connectivityReceiver,
                                                ISessionManager sessionManager,
                                                IInfoManager infoManager,
                                                IActivityNavigationManager activityNavigationManager) {
        return new UserItemViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    @Provides
    @ViewScope
    UsersSearchViewModel providesUsersSearchViewModel(IAuthBusinessLayer authBusinessLayer,
                                                      ConnectivityReceiver connectivityReceiver,
                                                      ISessionManager sessionManager,
                                                      IInfoManager infoManager,
                                                      IActivityNavigationManager activityNavigationManager,
                                                      IUsersBusinessLayer usersBusinessLayer,
                                                      ILikesBusinessLayer likesBusinessLayer,
                                                      IFragmentNavigationManager fragmentNavigationManager) {
        return new UsersSearchViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager, usersBusinessLayer,
                likesBusinessLayer, fragmentNavigationManager);
    }

    @Provides
    @ViewScope
    SplashViewModel providesSplashViewModel(IAuthBusinessLayer authBusinessLayer,
                                            ConnectivityReceiver connectivityReceiver,
                                            ISessionManager sessionManager,
                                            IInfoManager infoManager,
                                            IActivityNavigationManager activityNavigationManager) {
        return new SplashViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    @Provides
    @ViewScope
    DrawerItemViewModel providesDrawerItemViewModel(IAuthBusinessLayer authBusinessLayer,
                                                    ConnectivityReceiver connectivityReceiver,
                                                    ISessionManager sessionManager,
                                                    IInfoManager infoManager,
                                                    IActivityNavigationManager activityNavigationManager) {
        return new DrawerItemViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    @Provides
    @ViewScope
    FriendsViewModel providesFriendsViewModel(IAuthBusinessLayer authBusinessLayer,
                                              ConnectivityReceiver connectivityReceiver,
                                              ISessionManager sessionManager,
                                              IInfoManager infoManager,
                                              IActivityNavigationManager activityNavigationManager,
                                              IFriendsBusinessLayer friendsBusinessLayer) {
        return new FriendsViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager, friendsBusinessLayer);
    }

    @Provides
    @ViewScope
    FollowersViewModel providesFollowersViewModel(IAuthBusinessLayer authBusinessLayer,
                                                  ConnectivityReceiver connectivityReceiver,
                                                  ISessionManager sessionManager,
                                                  IInfoManager infoManager,
                                                  IActivityNavigationManager activityNavigationManager,
                                                  IUsersBusinessLayer usersBusinessLayer) {
        return new FollowersViewModel(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager, usersBusinessLayer);
    }
}
