package com.example.ivitruk.vkadditionalclient.di.scope;

import javax.inject.Scope;
import javax.inject.Singleton;

@Scope
@Singleton
public @interface ApplicationScope {
}
