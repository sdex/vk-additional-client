package com.example.ivitruk.vkadditionalclient.di.scope;

import javax.inject.Scope;

@Scope
public @interface BusinessScope {
}
