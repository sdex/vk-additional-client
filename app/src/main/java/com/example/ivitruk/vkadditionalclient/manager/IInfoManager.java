package com.example.ivitruk.vkadditionalclient.manager;

import com.example.ivitruk.vkadditionalclient.viewmodel.BaseViewModel;

public interface IInfoManager extends IManager {

    void showProgress();

    void hideProgress();

    void showMessage(String message);

    void showErrorMessage(Throwable t, BaseViewModel viewModel);

    void hideMessage();
}
