package com.example.ivitruk.vkadditionalclient.manager.impl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.view.activity.BaseActivity;
import com.example.ivitruk.vkadditionalclient.viewmodel.BaseViewModel;

import java.io.IOException;

public class InfoManager implements IInfoManager {

    private ProgressDialog progress;

    private Snackbar snackbar;

    @Override
    public void showProgress() {
        if (activity() == null) {
            return;
        }
        hideProgress();
        progress = new ProgressDialog(activity());
        progress.setCanceledOnTouchOutside(false);
        progress.setMessage(activity().getString(R.string.loading));
        progress.show();
    }

    @Override
    public void hideProgress() {
        if (activity() == null) {
            return;
        }
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void showMessage(String message) {
        if (activity() == null) {
            return;
        }
        hideMessage();
        BaseActivity activity = App.getCurrentActivity();
        if (activity != null) {
            snackbar = Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void showErrorMessage(Throwable t, BaseViewModel viewModel) {
        if (activity() == null) {
            return;
        }
        hideMessage();
        String message;
        boolean showRetry = false;
        if (t instanceof IOException) {
            message = activity().getString(R.string.connection_problem);
            showRetry = true;
        } else {
            message = t.getMessage();
        }

        BaseActivity activity = App.getCurrentActivity();
        if (activity != null) {
            snackbar = Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE);
            if (showRetry && viewModel != null) {
                snackbar.setAction(activity.getString(R.string.retry).toUpperCase(), v -> viewModel.refreshData());
            }
            snackbar.show();
        }
    }

    @Override
    public void hideMessage() {
        if (activity() == null) {
            return;
        }
        if (snackbar != null) {
            snackbar.dismiss();
            snackbar = null;
        }
    }

    private Activity activity() {
        return App.getCurrentActivity();
    }
}
