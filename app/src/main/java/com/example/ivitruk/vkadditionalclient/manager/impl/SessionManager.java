package com.example.ivitruk.vkadditionalclient.manager.impl;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.manager.data.Session;
import com.example.ivitruk.vkadditionalclient.util.StringUtils;

public class SessionManager implements ISessionManager {

    private Context context;

    private AccountManager accountManager;

    private final String accountType;

    public SessionManager(Context context) {
        this.context = context;
        accountManager = AccountManager.get(context);
        accountType = context.getString(R.string.account_type);
    }

    private final String KEY_ACCESS_TOKEN = "access_token";
    private final String KEY_EXPIRES_IN = "expires_in";
    private final String KEY_USER_ID = "user_id";
    private final String KEY_ACCESS_TIME = "access_time";

    @Override
    public void saveSession(Session session) {
        Account account = new Account(context.getString(R.string.app_name), accountType);
        accountManager.addAccountExplicitly(account, null, null);
        accountManager.setUserData(account, KEY_ACCESS_TOKEN, session.getAccessToken());
        accountManager.setUserData(account, KEY_USER_ID, session.getUserId());
        accountManager.setUserData(account, KEY_EXPIRES_IN, String.valueOf(session.getExpiresIn()));
        accountManager.setUserData(account, KEY_ACCESS_TIME, String.valueOf(session.getAccessTime()));
    }

    @Override
    public void deleteSession() {
        Account[] accounts = accountManager.getAccountsByType(accountType);
        for (Account account : accounts) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                accountManager.removeAccountExplicitly(account);
            } else {
                accountManager.removeAccount(account, null, null);
            }
        }
    }


    @Override
    public boolean isValidSession() {
        return getSession().isValid();
    }

    @Override
    public String getAccessToken() {
        return getSession().getAccessToken();
    }

    @NonNull
    private Session getSession() {
        Session session = new Session();
        Account[] accounts = accountManager.getAccountsByType(accountType);
        if (accounts.length > 0) {
            Account account = accounts[0];
            session.setAccessToken(StringUtils.safeString(accountManager.getUserData(account, KEY_ACCESS_TOKEN), DEFAULT_PREF_STRING));
            session.setUserId(StringUtils.safeString(accountManager.getUserData(account, KEY_USER_ID), DEFAULT_PREF_STRING));
            session.setAccessTime(StringUtils.safeLong(accountManager.getUserData(account, KEY_ACCESS_TIME), DEFAULT_PREF_LONG));
            session.setExpiresIn(StringUtils.safeLong(accountManager.getUserData(account, KEY_EXPIRES_IN), DEFAULT_PREF_LONG));

        } else {
            session.setAccessToken(DEFAULT_PREF_STRING);
            session.setUserId(DEFAULT_PREF_STRING);
            session.setExpiresIn(DEFAULT_PREF_LONG);
            session.setAccessTime(DEFAULT_PREF_LONG);
        }
        return session;
    }
}
