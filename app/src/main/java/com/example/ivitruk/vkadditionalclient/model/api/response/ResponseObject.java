package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

public abstract class ResponseObject<T> {
    @SerializedName("response")
    private final T response;

    @SerializedName("error")
    private final Error error;

    public ResponseObject(T response, Error error) {
        this.response = response;
        this.error = error;
    }

    public T getResponse() {
        return response;
    }

    public Error getError() {
        return error;
    }
}
