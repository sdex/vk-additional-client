package com.example.ivitruk.vkadditionalclient.model.business.impl;

import com.example.ivitruk.vkadditionalclient.model.datalistener.BaseDataListener;
import com.example.ivitruk.vkadditionalclient.model.business.IBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.response.Error;
import com.example.ivitruk.vkadditionalclient.model.api.response.ResponseObject;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseBusinessLayer<T extends BaseDataListener, A> implements IBusinessLayer<T> {

    protected A api;

    protected T dataListener;

    private CompositeSubscription compositeSubscription;

    protected ISessionManager sessionManager;

    public BaseBusinessLayer(A api, ISessionManager sessionManager) {
        compositeSubscription = new CompositeSubscription();
        this.api = api;
        this.sessionManager = sessionManager;
    }

    @Override
    public void setDataListener(T dataListener) {
        this.dataListener = dataListener;
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    protected <C extends ResponseObject> void doMethod(Observable<C> observable,
                                                       Action1<C> actionSuccessVk,
                                                       Action1<Throwable> actionError) {
        doMethod(observable, actionSuccessVk, actionError,
                c -> dataListener.onUnauthorizedError());
    }

    protected <C extends ResponseObject> void doMethod(Observable<C> observable,
                                                       Action1<C> actionSuccessVk,
                                                       Action1<Throwable> actionError,
                                                       Action1<C> actionUnauthorizedError) {
        if (!sessionManager.isValidSession()) {
            dataListener.onUnauthorizedError();
            return;
        }

        Action1<C> successAction = c -> {
            Error error = c.getError();
            if (error != null) {
                if (error.getErrorCode() == ApiData.ApiErrorCodes.AUTHORIZATION_FAILED) {
                    actionUnauthorizedError.call(c);
                } else {
                    actionError.call(new Throwable(error.getErrorMessage()));
                }
            } else {
                actionSuccessVk.call(c);
            }
        };

        compositeSubscription.add(observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(successAction, actionError));
    }
}
