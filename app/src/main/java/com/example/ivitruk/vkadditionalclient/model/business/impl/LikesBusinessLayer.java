package com.example.ivitruk.vkadditionalclient.model.business.impl;

import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.ILikesApi;
import com.example.ivitruk.vkadditionalclient.model.business.ILikesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.LikesDataListener;

public class LikesBusinessLayer extends BaseBusinessLayer<LikesDataListener, ILikesApi> implements ILikesBusinessLayer {
    public LikesBusinessLayer(ILikesApi api, ISessionManager sessionManager) {
        super(api, sessionManager);
    }

    @Override
    public void add(String type, long ownerId, long itemId) {
        doMethod(api.add(type, ownerId, itemId),
                responseObject -> dataListener.onAddLikeSuccess(),
                throwable -> dataListener.onAddLikeError(throwable));
    }
}
