package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.manager.data.Session;

public class AuthDataListener extends BaseDataListener {

    public AuthDataListener() {
        super(null);
    }

    //login
    public void onLoginSuccess(Session session) {

    }

    public void onLoginError(Throwable t) {
        
    }

    //logout
    public void onLogoutSuccess() {

    }

    public void onLogoutError(Throwable t) {

    }
}
