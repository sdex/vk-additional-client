package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.model.api.response.Message;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;

import java.util.List;
import java.util.Map;

public abstract class MessagesDataListener extends BaseDataListener {

    public MessagesDataListener(IAuthBusinessLayer authBusinessLayer) {
        super(authBusinessLayer);
    }

    //messages.get
    public void onGetMessagesSuccess(List<Message> messages) {
    }

    public void onGetMessagesError(Throwable t) {
    }

    //message.getById
    public void onGetMessagesByIdSuccess(List<Message> messages) {
    }

    public void onGetMessagesByIdError(Throwable t) {
    }

    //restore
    public void onRestoreSuccess(Map<Long, Integer> success) {
    }

    public void onRestoreError(Throwable t) {
    }

    //getHistory
    public void onGetHistorySuccess(List<Message> messages) {
    }

    public void onGetHistoryError(Throwable t) {
    }
}
