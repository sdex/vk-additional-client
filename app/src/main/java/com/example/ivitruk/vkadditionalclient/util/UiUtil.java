package com.example.ivitruk.vkadditionalclient.util;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

public class UiUtil {

    public static void showImage(ImageView iv, String url) {
        Glide.with(iv.getContext())
                .load(url)
                .asBitmap()
                .into(new BitmapImageViewTarget(iv) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(iv.getContext().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        iv.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    public static AlertDialog.Builder newConfirmationDialog(Context context,
                                                            String title,
                                                            String text,
                                                            String positiveButtonText,
                                                            String negativeButtonText,
                                                            DialogInterface.OnClickListener positiveButtonOnClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(text);
        builder.setCancelable(false);
        builder.setPositiveButton(positiveButtonText, positiveButtonOnClickListener);
        builder.setNegativeButton(negativeButtonText, (dialog, which) -> {
        });
        return builder;
    }

    public static AlertDialog.Builder newMessageDialog(Context context,
                                                            String title,
                                                            String text,
                                                            String positiveButtonText,
                                                            DialogInterface.OnClickListener positiveButtonOnClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(text);
        builder.setPositiveButton(positiveButtonText, positiveButtonOnClickListener);
        return builder;
    }

    private UiUtil() {
        throw new AssertionError();
    }
}
