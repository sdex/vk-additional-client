package com.example.ivitruk.vkadditionalclient.view.activity;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.di.module.ApiModule;
import com.example.ivitruk.vkadditionalclient.di.module.BusinessModule;
import com.example.ivitruk.vkadditionalclient.di.module.ViewModelModule;
import com.example.ivitruk.vkadditionalclient.viewmodel.BaseViewModel;

public abstract class BaseActivity<BVM extends BaseViewModel,
        VDB extends ViewDataBinding> extends AppCompatActivity {

    protected ViewModelComponent viewModelComponent;

    protected BVM viewModel;

    protected VDB binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.setCurrentActivity(this);
        viewModelComponent = App.getAppComponent()
                .plus(new ApiModule())
                .plus(new BusinessModule())
                .plus(new ViewModelModule(this));
        viewModel = initViewModel();
        viewModel.create();
        binding = initBinding();

        viewModel.refreshData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.setCurrentActivity(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.destroy();
        viewModelComponent = null;
    }

    @NonNull
    public abstract BVM initViewModel();

    @NonNull
    public abstract VDB initBinding();

}
