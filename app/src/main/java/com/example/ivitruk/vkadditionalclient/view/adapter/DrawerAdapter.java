package com.example.ivitruk.vkadditionalclient.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.DrawerItem;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.DrawerItemBinding;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.view.viewholder.DrawerViewHolder;

public class DrawerAdapter extends BaseRvAdapter<DrawerItem, DrawerViewHolder> {

    public DrawerAdapter(ViewModelComponent viewModelComponent) {
        super(viewModelComponent);
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DrawerItemBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(parent.getContext()),
                        R.layout.drawer_item, parent, false);
        return new DrawerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {
        holder.bind(items.get(position));
    }
}
