package com.example.ivitruk.vkadditionalclient.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.model.api.response.Message;
import com.example.ivitruk.vkadditionalclient.databinding.MessageItemBinding;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.view.viewholder.MessageItemViewHolder;

public class MessagesAdapter extends BaseRvAdapter<Message, MessageItemViewHolder> {

    public MessagesAdapter(ViewModelComponent viewModelComponent) {
        super(viewModelComponent);
    }

    @Override
    public MessageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MessageItemBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(parent.getContext()),
                        R.layout.message_item, parent, false);
        return new MessageItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MessageItemViewHolder holder, int position) {
        holder.bind(items.get(position));
    }
}
