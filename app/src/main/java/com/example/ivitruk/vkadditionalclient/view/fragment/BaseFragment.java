package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.di.module.ApiModule;
import com.example.ivitruk.vkadditionalclient.di.module.BusinessModule;
import com.example.ivitruk.vkadditionalclient.di.module.ViewModelModule;
import com.example.ivitruk.vkadditionalclient.viewmodel.BaseViewModel;

public abstract class BaseFragment<BVM extends BaseViewModel, VDB extends ViewDataBinding> extends Fragment {

    protected ViewModelComponent viewModelComponent;

    protected BVM viewModel;

    protected VDB binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModelComponent = App.getAppComponent()
                .plus(new ApiModule())
                .plus(new BusinessModule())
                .plus(new ViewModelModule(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = initViewModel();
        viewModel.create();
        binding = initBinding(inflater, container, savedInstanceState);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.refreshData();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModelComponent = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @NonNull
    public abstract BVM initViewModel();

    @NonNull
    public abstract VDB initBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);
}
