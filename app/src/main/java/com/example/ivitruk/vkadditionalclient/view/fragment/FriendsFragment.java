package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.FriendsFragmentBinding;
import com.example.ivitruk.vkadditionalclient.view.adapter.UsersAdapter;
import com.example.ivitruk.vkadditionalclient.viewmodel.FriendsViewModel;

import javax.inject.Inject;

public class FriendsFragment extends BaseFragment<FriendsViewModel, FriendsFragmentBinding> {

    @Inject
    FriendsViewModel friendsViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel.setupRecyclerView(binding.friendsRv, new UsersAdapter(viewModelComponent));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_global, menu);
        super.onCreateOptionsMenu(menu, inflater);

        SearchView searchView = (SearchView)menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                viewModel.setSearchQuery(newText);
                viewModel.refreshData();
                return false;
            }
        });
    }

    @NonNull
    @Override
    public FriendsViewModel initViewModel() {
        viewModelComponent.inject(this);
        return friendsViewModel;
    }

    @NonNull
    @Override
    public FriendsFragmentBinding initBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FriendsFragmentBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()),
                        R.layout.friends_fragment, container, false);
        binding.setViewModel(viewModel);
        return binding;
    }
}
