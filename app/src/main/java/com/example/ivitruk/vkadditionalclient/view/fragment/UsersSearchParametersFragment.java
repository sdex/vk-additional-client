package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.os.Bundle;

import com.example.ivitruk.vkadditionalclient.R;

import me.philio.preferencecompatextended.PreferenceFragmentCompat;

public class UsersSearchParametersFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.users_search_parameters);
    }
}
