package com.example.ivitruk.vkadditionalclient.view.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.example.ivitruk.vkadditionalclient.util.NetworkUtil;

public class ConnectivityReceiver extends BroadcastReceiver {

    private IntentFilter connectivityIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

    private ConnectivityReceiver.Callback callback;

    public interface Callback {
        void onReceive();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (NetworkUtil.isNetworkAvailable(context)) {
            if (callback != null) {
                if (!isInitialStickyBroadcast()) {
                    callback.onReceive();
                }
            }
        }
    }

    public void register(ConnectivityReceiver.Callback callback, Context context) {
        this.callback = callback;
        context.registerReceiver(this, connectivityIntentFilter);
    }

    public void unregister(Context context) {
        context.unregisterReceiver(this);
        callback = null;
    }
}