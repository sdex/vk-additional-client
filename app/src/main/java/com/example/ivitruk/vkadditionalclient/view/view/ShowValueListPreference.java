package com.example.ivitruk.vkadditionalclient.view.view;

import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.util.AttributeSet;

public class ShowValueListPreference extends ListPreference {
    public ShowValueListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public ShowValueListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ShowValueListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShowValueListPreference(Context context) {
        super(context);
    }

    @Override
    public void setValue(String value) {
        super.setValue(value);
        setSummary(getEntry());
    }
}
