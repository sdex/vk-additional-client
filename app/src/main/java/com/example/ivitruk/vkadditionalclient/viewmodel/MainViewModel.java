package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;

import com.example.ivitruk.vkadditionalclient.DrawerItem;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IFragmentNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.adapter.DrawerAdapter;
import com.example.ivitruk.vkadditionalclient.view.fragment.FollowersFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.FriendsFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.MessagesFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.UsersSearchFragment;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends BaseViewModel {

    public IFragmentNavigationManager fragmentNavigationManager;

    public MainViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                         ConnectivityReceiver connectivityReceiver,
                         ISessionManager sessionManager,
                         IInfoManager infoManager,
                         IActivityNavigationManager activityNavigationManager,
                         IFragmentNavigationManager fragmentNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
        this.fragmentNavigationManager = fragmentNavigationManager;
    }

    public void setupDrawer(DrawerLayout drawerLayout,
                            RecyclerView recyclerView,
                            ActionBar actionBar,
                            ViewModelComponent viewModelComponent,
                            FragmentManager fragmentManager) {
        DrawerAdapter drawerAdapter = new DrawerAdapter(viewModelComponent);
        List<DrawerItem> items = new ArrayList<>();
        DrawerItem defaultItem = new DrawerItem(context.getString(R.string.friends),
                v -> {
                    fragmentNavigationManager.replaceFragment(R.id.main_container, FriendsFragment.class, fragmentManager);
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    actionBar.setTitle(context.getString(R.string.friends));
                });
        items.add(defaultItem);
        items.add(new DrawerItem(context.getString(R.string.followers),
                v -> {
                    fragmentNavigationManager.replaceFragment(R.id.main_container, FollowersFragment.class, fragmentManager);
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    actionBar.setTitle(context.getString(R.string.followers));
                }));
        items.add(new DrawerItem(context.getString(R.string.users_search),
                v -> {
                    fragmentNavigationManager.replaceFragment(R.id.main_container, UsersSearchFragment.class, fragmentManager);
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    actionBar.setTitle(context.getString(R.string.users_search));
                }));
       /* items.add(new DrawerItem(context.getString(R.string.messages),
                v -> {
                    fragmentNavigationManager.replaceFragment(R.id.main_container, MessagesFragment.class, fragmentManager);
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    actionBar.setTitle(context.getString(R.string.messages));
                }));*/
        items.add(new DrawerItem(context.getString(R.string.logout),
                v -> {
                    infoManager.showProgress();
                    authBusinessLayer.logout();
                }));
        drawerAdapter.setItems(items);
        recyclerView.setAdapter(drawerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        defaultItem.getOnClickListener().onClick(null);
    }
}
