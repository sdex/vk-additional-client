package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.util.UiUtil;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

public class UserItemViewModel extends BaseViewModel {

    private User user;

    public UserItemViewModel(Context context,
                             IAuthBusinessLayer authBusinessLayer,
                             ConnectivityReceiver connectivityReceiver,
                             ISessionManager sessionManager,
                             IInfoManager infoManager,
                             IActivityNavigationManager activityNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver, sessionManager, infoManager, activityNavigationManager);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        notifyChange();
    }

    public void onClickUser(View v) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("vkontakte://profile/%d", user.getId())));
        v.getContext().startActivity(intent);
    }

    public void onClickMore(View v) {
        PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
        popupMenu.inflate(R.menu.menu_user_item);
        popupMenu.show();
    }

    @BindingAdapter("bind:like")
    public static void loadImage(ImageView imageView, String url) {
        UiUtil.showImage(imageView, url);
    }
}
